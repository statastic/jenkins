FROM jenkins/jenkins:alpine3.19-jdk21

USER root

COPY files/plugins.txt /plugins.txt
COPY files/default.casc /jenkins.casc
COPY files/statastic-rootCA.der /statastic-rootCA.der
COPY files/statastic-rootCA.crt /usr/local/share/ca-certificates/statastic-rootCA.crt

RUN update-ca-certificates \
 && keytool -noprompt \
    -import -trustcacerts -cacerts -alias statastic \
    -storepass changeit \
    -file /statastic-rootCA.der
USER jenkins

RUN  jenkins-plugin-cli --plugin-file /plugins.txt

ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false
ENV CASC_JENKINS_CONFIG /jenkins.casc

WORKDIR $JENKINS_HOME